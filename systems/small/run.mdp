
; POSITION RESTRAIN

; RUN CONTROL
integrator           = sd           
dt                   = 0.002         ; Time step (ps).
nsteps               = 10000 ;10000000      ; 20 ns.      
comm-mode            = Linear        ; Remove center of mass translation.
comm-grps            = System ;Protein Non-Protein

; OUTPUT CONTROL
nstxout-compressed   = 5000         ; Write frame every 10.000 ps.

; NEIGHBOUR SEARCHING
cutoff-scheme        = Verlet        ; Related params are inferred by Gromacs.

; BOND PARAMETERS
constraints          = all-bonds       ; Constraints applied to all bonds
constraint_algorithm = lincs         ; Holonomic constraints.
lincs_iter           = 2             ; Related to accuracy of LINCS.
lincs_order          = 4             ; Related to accuracy of LINCS.

; ELECTROSTATICS
coulombtype          = PME           ; Use Particle Mesh Ewald.
rcoulomb             = 1.0           ; Pavel: CHARMM was calibrated for 1.2 nm.
fourierspacing       = 0.125          ; Pavel: set this to 0.125 for CHARMM.

; VAN DER WAALS
vdwtype              = cut-off       ; Twin range cut-off with nblist cut-off.
rvdw                 = 1.0           ; Pavel: Amber was calibrated for 1.0 nm.
vdw-modifier         = Potential-shift-Verlet  ; Pavel: specific for AMBER.
DispCorr             = EnerPres ; account for cut-off vdW scheme

; TEMPERATURE COUPLING
tcoupl               = v-rescale
tc-grps              = SYSTEM
tau-t                = 0.5           ; Berk: change from 0.1 to 0.5.
ref-t                = 300           ; Reference temp. (K) (for each group).

; PRESSURE COUPLING
pcoupl               = C-rescale
pcoupltype           = isotropic     ; Uniform scaling of box.
tau_p                = 5.0           ; Berk: better to change from 2.0 to 5.0.
ref_p                = 1.0           ; Reference pressure (bar).
compressibility      = 4.5e-05       ; Isothermal compressbility of water.
refcoord_scaling     = all           ; Required with position restraints.

; PERIODIC BOUNDARY CONDITION
pbc                  = xyz           ; To keep molecule(s) in box.




; Free energy section
free-energy                    = yes
nstdhdl                        = 50 ; writing dhdl every 0.1 ps
sc-function                    = beutler ; Default choice in GROMACS
sc-sigma                       = 0.3 ; Default value
sc-alpha                       = 0.5 ; As recommended by Soft-Core Potentials in TI, Steinbrecher et al., doi 10.1002/jcc.21909
sc-power                       = 1 ; Default value
sc-coul                        = yes ; Also use soft-core for electrostatics
calc-lambda-neighbors          = -1 ; -1 is suitable for MBAR
init-lambda-state              = 6
coul-lambdas                   = 0.         0.02       0.05       0.17857143 0.30714286 0.43571429 0.56428571 0.69285714 0.82142857 0.95       0.98       1.        
vdw-lambdas                    = 0.         0.02       0.05       0.17857143 0.30714286 0.43571429 0.56428571 0.69285714 0.82142857 0.95       0.98       1.        