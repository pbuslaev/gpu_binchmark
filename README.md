# GPU benchmark

This is a collection of files and data descibing the GROMACS FEP code benchmarking on a cluster with 4 **NVIDIA GeForce GTX 1080 Ti** GPUs and 2 processors with 12 **Intel(R) Xeon(R) CPU E5-2687W v4 @ 3.00GHz** cores each. The benchmark is performed on the *Tyk2* protein system. Protein input is taken from [pmx benchmark](https://github.com/deGrootLab/pmx/tree/master/protLig_benchmark/tyk2/protein_amber). `pmx` was used to generate hybrid topology between ligands [**lig_ejm_31**](https://github.com/openforcefield/protein-ligand-benchmark/tree/0.2.1/data/2020-02-07_tyk2/02_ligands/lig_ejm_31/crd) and [**lig_ejm_43**](https://github.com/openforcefield/protein-ligand-benchmark/tree/0.2.1/data/2020-02-07_tyk2/02_ligands/lig_ejm_43/crd) parameterized with `openff-2.1.0.offxml`. More details on the systems benchmarked are provided in the following section.

## Simulated systems

| System | # atoms | # ligand atoms | # perturbed atoms | # waters |
| ------ | ------- | -------------- | ----------------- | -------- |
| Small  | 23057 | 43 | 43 | 6099 |
| Medium | 54807 | 43 | 43 | 16663 |
| Large  | 105317 | 43 | 43 | 33469 |

Input structures and topologies for simulated systems can be found in [small](https://gitlab.astex-technology.com/pbuslaev/gpu_benchmark/-/tree/main/systems/small) for system *Small*, [medium](https://gitlab.astex-technology.com/pbuslaev/gpu_benchmark/-/tree/main/systems/medium) for system *Medium*, and [large](https://gitlab.astex-technology.com/pbuslaev/gpu_benchmark/-/tree/main/systems/large) for system *Large*. Input structures are `6_npt.gro`, which were first minimized and then equilibrated in NVT and NPT ensembles for 100 ps.

## Simulation parameters
The benchmarking was performed with and without FEP. The input `.mdp` files can be found [here](https://gitlab.astex-technology.com/pbuslaev/gpu_benchmark/-/tree/main/mdp_files). The list of parameters used for different types of runs and corresponding `.mdp`-file names were:

| FEP | Integrator | `.mdp` |
| --- | ---------- | ------ |
| On  | md         | run_fep_on_md.mdp |
| On  | sd         | run_fep_on_sd.mdp |
| Off | md         | run_fep_off_md.mdp |

In simulations with FEP on, lambda was set to 6, to ensure that Hamiltonian interpolation is done.

## Simulation set up (mdrun options)
All simulations were run on a single GPU with 1, 2, 4, 8, and 16 CPUs assigned to the run as `omp` threads. For each possible combination of system and number of `omp` threads used, the following `gmx mdrun` options were used in combination with listed `.mdp` files:

| Run name | `.mdp` | -update |
| -------- | ------ | ------- |
| run_system_NAME_nt_X_fep_on_md_update_gpu_repeat_Y | run_fep_on_md.mdp | gpu |
| run_system_NAME_nt_X_fep_on_md_update_cpu_repeat_Y | run_fep_on_md.mdp | cpu |
| run_system_NAME_nt_X_fep_on_sd_update_cpu_repeat_Y | run_fep_on_cd.mdp | cpu |
| run_system_NAME_nt_X_fep_off_md_update_gpu_repeat_Y | run_fep_off_md.mdp | gpu |
| run_system_NAME_nt_X_fep_off_md_update_cpu_repeat_Y | run_fep_off_md.mdp | cpu |

All runs were submitted with the followin run script:
```
gmx mdrun -nb gpu -pme gpu -bonded gpu -update <cpu/gpu> -deffnm <run_name> -nsteps 100000 -resethway -nt X &
```
Each run was repeated 5 times.

## Benchmark results

All the output `.log` files can be found [here](https://gitlab.astex-technology.com/pbuslaev/gpu_benchmark/-/tree/main/runs_data). The benchmark was analysed by the reported performance in terms of `ns/day`. Performances were averaged between 5 repeats. The summary of the results is presented in the following plots.

![benchmark](figures/out.png)
![benchmark](figures/out_full.png)

## Comparison with other software

Additionally we ran standard MD simulations of the exact same systems with openmm on the same GPU. We also present indirect performance comparison with Desmond based on [public performance data](https://www.schrodinger.com/products/desmond). For Desmond performance we used reported values for Tesls T4 GPU (320 ns/day for 23857 atoms, 80 ns/day for 93337 atoms). Based on [technical specification](https://versus.com/en/nvidia-geforce-gtx-1080-ti-vs-nvidia-tesla-t4/tflops) for T4 and GTX1080 Ti, we expect the performance of GTX1080 Ti to be 1.36 times higher, leading to performance of 437 ns/day for 23857 atoms and 110 ns/day for 93337, and the projected performances for small and large systems are therefore **450 ns/day** **97 ns/day**.

| System | Desmond | GROMACS (1 GPU + 1 CPU) | GROMACS (1GPU + 8 CPUs) | OpenMM |
| --- | --- | --- | --- | --- |
| Small | 450 ns/day | 378 ns/day | 415 ns/day| 281 ns/day |
| Medium | x | 171 ns/day | 205 ns/day | 124 ns/day |
| Large | 97 ns/day | 87 ns/day | 110 ns/day  | 65 ns/day |

