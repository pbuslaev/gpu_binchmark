from openmm.app import *
from openmm import *
from openmm.unit import *
from sys import stdout
import time
import pandas as pd

system_path = "/home/pbuslaev/projects/gpu_benchmark/systems"

systems = ["small", "medium", "large"]

out_path = "/home/pbuslaev/projects/gpu_benchmark/runs"

out_reduced_path = "/home/pbuslaev/projects/gpu_benchmark/runs_data"

performance = []

def run(system: str, topology: str, include: str, output: str) -> float:
    """
    Run openmm and get the performance
    """
    gro = GromacsGroFile(system)
    top = GromacsTopFile(
        topology,
        periodicBoxVectors=gro.getPeriodicBoxVectors(),
        includeDir=include,
    )
    system = top.createSystem(
        nonbondedMethod=PME,
        nonbondedCutoff=1*nanometer,
        constraints=HBonds
    )
    system.addForce(MonteCarloBarostat(1*bar, 300*kelvin))
    integrator = LangevinIntegrator(300*kelvin, 1/picosecond, 0.002*picoseconds)
    simulation = Simulation(top.topology, system, integrator)
    simulation.context.setPositions(gro.positions)
    simulation.minimizeEnergy()
    simulation.reporters.append(PDBReporter(output, 10000))
    simulation.reporters.append(
        StateDataReporter(stdout, 10000, step=True,
        potentialEnergy=True, temperature=True)
    )
    simulation.step(10000)
    st = time.time()
    # run 100 ps
    simulation.step(50000)
    end = time.time()
    return 0.1 * 3600 * 24 / (end - st)

if __name__ == "__main__":
    system = sys.argv[1]
    repeat = sys.argv[2]
    p = run(
        f"{system_path}/{system}/6_npt.gro",
        f"{system_path}/{system}/topol.top",
        f"{system_path}/{system}",
        f"{out_path}/openmm_{system}_{repeat}.pdb"
    )
    print(p)
