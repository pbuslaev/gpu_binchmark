import os
from subprocess import Popen, PIPE
from itertools import product
import shutil
import pandas as pd
import glob
import seaborn as sns
import matplotlib.pyplot as plt
import numpy as np

mdp_path = "/home/pbuslaev/projects/gpu_benchmark/mdp_files"

run_setups = [
    {
        "mdp": "run_fep_on_md.mdp",
        "fep": True,
        "update": "gpu",
    },
    {
        "mdp": "run_fep_on_md.mdp",
        "fep": True,
        "update": "cpu",
    },
    {
        "mdp": "run_fep_on_sd.mdp",
        "fep": True,
        "update": "cpu",
    },
    {
        "mdp": "run_fep_off_md.mdp",
        "fep": False,
        "update": "gpu",
    },
    {
        "mdp": "run_fep_off_md.mdp",
        "fep": False,
        "update": "cpu",
    }
]

system_path = "/home/pbuslaev/projects/gpu_benchmark/systems"

systems = ["small", "medium", "large"]

nomp = [1, 2, 4, 8, 16]

out_path = "/home/pbuslaev/projects/gpu_benchmark/runs"

out_reduced_path = "/home/pbuslaev/projects/gpu_benchmark/runs_data"

fig_path = "/home/pbuslaev/projects/gpu_benchmark/figures"

run_classes = {
    "fep on, md, update gpu": 0,
    "fep on, md, update cpu": 1,
    "fep on, sd, update cpu": 2,
    "fep off, md, update gpu": 3,
    "fep off, md, update cpu": 4,
}

def generate_run_name(
    system: str, nt: int, run_setup: dict, repeat: int
) -> str:
    """
    Generate run name in the form:
        run_system_NAME_nt_X_fep_on_md_update_gpu_repeat_Y
    """
    fep = "on" if run_setup["fep"] else "off"
    update = run_setup["update"]
    integrator = run_setup["mdp"].split(".")[0].split("_")[-1]
    return f"run_system_{system}_nt_{nt}_fep_{fep}_{integrator}_update_{update}_repeat_{repeat}"

def prepare_runs() -> None:
    """
    Create folders and prepare runs
    """
    for system, nt, run_setup, repeat in product(systems, nomp, run_setups, list(range(1,6))):
        name = generate_run_name(system, nt, run_setup, repeat)
        out_folder = f"{out_path}/{name}"
        if not os.path.exists(out_folder):
            os.makedirs(out_folder)
        command = (
            f"gmx grompp -f {mdp_path}/{run_setup['mdp']} "
            + f"-c {system_path}/{system}/6_npt.gro "
            + f"-p {system_path}/{system}/topol.top "
            + f"-o {out_folder}/{name}.tpr"
        )
        p = Popen(command.split(), stderr = PIPE, stdout = PIPE)
        p.wait()
        assert p.returncode == 0, f'Running {command} failed'

def run_benchmark() -> None:
    """
    Run benchmark
    """
    for system, nt, run_setup, repeat in product(systems, nomp, run_setups, list(range(1,6))):
        name = generate_run_name(system, nt, run_setup, repeat)
        print(name)
        out_folder = f"{out_path}/{name}"
        command = (
            f"gmx mdrun -nb gpu -pme gpu -bonded gpu "
            + f"-update {run_setup['update']} -deffnm {out_folder}/{name} "
            + f"-nsteps 100000 -resethway -nt {nt}"
        )
        p = Popen(command.split(), stderr = PIPE, stdout = PIPE)
        p.wait()
        assert p.returncode == 0, f'Running {command} failed'
        print("completed")

        shutil.copyfile(
            f"{out_folder}/{name}.log", f"{out_reduced_path}/{name}.log"
        )

def get_performance_from_file(logfile: str) -> float:
    """
    Get performance after run is finished
    """
    with open(logfile, "r") as f:
        for line in f:
            if line.startswith("Performance"):
                return float(line.split()[1])

def parse_name(fname: str) -> tuple:
    """
    Parse file name and get nt, repeat, system, and class
    """
    split = fname.split("/")[-1].split(".")[0].split("_")
    nt = int(split[4])
    repeat = int(split[-1])
    system = split[2]
    run_class = run_classes[
        f"fep {split[6]}, {split[7]}, update {split[9]}"
    ]
    return (nt, repeat, system, run_class)

def get_performance() -> pd.DataFrame:
    """
    Get performance data
    """
    files = glob.glob(f"{out_reduced_path}/*.log")
    data = []
    for f in files:
        nt, repeat, system, run_class = parse_name(f)
        performance = get_performance_from_file(f)
        data.append([
            list(run_classes.keys())[
                list(run_classes.values()).index(run_class)
            ],
            repeat,
            nt,
            system,
            performance
        ])
    df = pd.DataFrame(data, columns = ["class", "repeat", "nt", "system", "performance"])
    df['mean_performance'] = df.groupby(["class", "system", "nt"]).performance.transform("mean")
    df['nt_log'] = df['nt']
    df = df.drop(columns=['repeat', 'performance'])
    df = df.drop_duplicates()
    df['nt_log'] = df['nt_log'].transform(lambda x: np.log2(x))
    plot = sns.lineplot(data = df, x='nt_log', y='mean_performance', hue='class', style='system')
    sns.scatterplot(data = df, x='nt_log', y='mean_performance', hue='class', style='system', legend = False)
    plot.set(xlabel = "Number of CPUs", ylabel = "Performance (ns/day)")
    sns.move_legend(plot, "upper left", bbox_to_anchor=(1, 1))
    plot.set_xticks(df.nt_log)
    plot.set_xticklabels(df.nt)
    fig = plot.get_figure()
    fig.savefig(f"{fig_path}/out.png", bbox_inches="tight")

    fig, axes = plt.subplots(1, 3, figsize = (20, 6))
    sns.lineplot(
        data = df[df["system"] == "small"],
        x='nt_log', y='mean_performance',
        hue='class',
        legend = False,
        ax = axes[0]
    )
    sns.scatterplot(
        data = df[df["system"] == "small"],
        x='nt_log',
        y='mean_performance',
        hue='class',
        legend = False,
        ax = axes[0]
    )
    axes[0].set_xticks(df.nt_log)
    axes[0].set_xticklabels(df.nt)
    axes[0].set(xlabel = "Number of CPUs", ylabel = "Performance (ns/day)", title = "System Small")
    sns.lineplot(
        data = df[df["system"] == "medium"],
        x='nt_log', y='mean_performance',
        hue='class',
        legend = False,
        ax = axes[1]
    )
    sns.scatterplot(
        data = df[df["system"] == "medium"],
        x='nt_log',
        y='mean_performance',
        hue='class',
        legend = False,
        ax = axes[1]
    )
    axes[1].set_xticks(df.nt_log)
    axes[1].set_xticklabels(df.nt)
    axes[1].set(xlabel = "Number of CPUs", ylabel = "Performance (ns/day)", title = "System Medium")
    sns.lineplot(
        data = df[df["system"] == "large"],
        x='nt_log', y='mean_performance',
        hue='class',
        ax = axes[2]
    )
    sns.scatterplot(
        data = df[df["system"] == "large"],
        x='nt_log',
        y='mean_performance',
        hue='class',
        legend = False,
        ax = axes[2]
    )
    axes[2].set_xticks(df.nt_log)
    axes[2].set_xticklabels(df.nt)
    axes[2].set(xlabel = "Number of CPUs", ylabel = "Performance (ns/day)", title = "System Large")

    sns.move_legend(axes[2], "upper left", bbox_to_anchor=(1, 1))

    fig.savefig(f"{fig_path}/out_full.png", bbox_inches="tight")

#prepare_runs()
#run_benchmark()
get_performance()
