from subprocess import PIPE, Popen
import pandas as pd

systems = ["small", "medium", "large"]

out_reduced_path = "/home/pbuslaev/projects/gpu_benchmark/runs_data"

performance = []
for s in systems:
    print(s)
    s_perf = 0
    n_replicas = 5
    for i in range(n_replicas):
        print(f"replica {i+1}")
        p = Popen(
            [
                "python", "scripts/run_benchmark_openmm.py", f"{s}", str(i)
            ],
            stdout=PIPE,
        )
        p.wait()
        output = p.communicate()[0]
        s_perf += float(output.split()[-1]) / n_replicas
        p.stdout.close()
    performance.append(s_perf)

df = pd.DataFrame(data = [performance], columns = systems)
df.to_csv(f"{out_reduced_path}/openmm_benchmark.csv")
